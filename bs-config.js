module.exports = {
    startPath: '/',
    port: 3000,
    files: [
        '*.{js}',
        '*.{css}'
    ],
    server: {
        baseDir: './dist',
        index: 'index.html'
    },
    browser: 'default',
    notify: false,
    ghostMode: {
        clicks: false,
        scroll: false,
        forms: {
            submit: false,
            inputs: false,
            toggles: false
        }
    }
};
