# Traffic Light Simulation

Simple traffic light simulation in ES6/2015, runnable in a browser

### Requirements

* Node >= 6.10.0

#### 1. Setup

`npm install`

#### 2. Build / Build & watch files

`npm run build`

`npm run build:watch`

#### 3. Running

`npm start`

Note: you can view in your browser at [http://localhost:3000/](http://localhost:3000/)

#### 4. Testing

`npm test`