'use strict';

import {TrafficLightColor} from './traffic-light-color';

export class TrafficLightSimulator {
    constructor(trafficLights, interval, delay) {
        this.trafficLights = trafficLights;
        this.trafficLightsLen = trafficLights ? trafficLights.length : 0;

        this.trafficLightInterval = interval;
        this.trafficLightIntervalBuffer = delay;

        this.currentSetIndex = 0;
        this.nextSetIndex = this.currentSetIndex + 1;
    }

    init() {
        if (this.trafficLightsLen < 2) {
            throw new Error('Traffic light simulation requires more than 1 set of lights');
        }

        this.changeLights(this.trafficLightInterval, this.trafficLightIntervalBuffer);
    }

    changeLights(delay, buffer) {
        this.trafficLights[this.currentSetIndex].color = TrafficLightColor.GREEN;

        console.log('Light: ' + this.currentSetIndex, this.trafficLights[this.currentSetIndex].color.toString());
        console.log('Light: ' + this.nextSetIndex, this.trafficLights[this.nextSetIndex].color.toString());

        // prepare
        setTimeout(() => {
            this.trafficLights[this.currentSetIndex].color = TrafficLightColor.ORANGE;

            console.log('Light: ' + this.currentSetIndex, this.trafficLights[this.currentSetIndex].color.toString());
            console.log('Light: ' + this.nextSetIndex, this.trafficLights[this.nextSetIndex].color.toString());

            // change
            setTimeout(() => {
                this.trafficLights[this.currentSetIndex].color = TrafficLightColor.RED;
                this.trafficLights[this.nextSetIndex].color = TrafficLightColor.GREEN;

                console.log('Light: ' + this.currentSetIndex, this.trafficLights[this.currentSetIndex].color.toString());
                console.log('Light: ' + this.nextSetIndex, this.trafficLights[this.nextSetIndex].color.toString() + '\n');

                this.currentSetIndex = (this.currentSetIndex === (this.trafficLightsLen - 1)) ?
                    0 : ++this.currentSetIndex;
                this.nextSetIndex = (this.nextSetIndex === (this.trafficLightsLen - 1)) ?
                    0 : ++this.nextSetIndex;

                this.changeLights(delay, buffer);
            }, buffer);
        }, delay);
    }
}
