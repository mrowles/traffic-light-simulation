'use strict';

export class TrafficLightColor {
    constructor(name) {
        this.name = name;
    }

    toString() {
        return `${this.name}`;
    }
}

TrafficLightColor.RED = new TrafficLightColor('RED');
TrafficLightColor.ORANGE = new TrafficLightColor('ORANGE');
TrafficLightColor.GREEN = new TrafficLightColor('GREEN');