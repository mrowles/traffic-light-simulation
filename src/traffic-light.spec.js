'use strict';

import {TrafficLight} from './traffic-light.js';
import {TrafficLightColor} from './traffic-light-color.js';

describe('TrafficLight', function () {
    it('should create a new Traffic Light', function () {
        let trafficLight = new TrafficLight();
        expect(trafficLight.color).toEqual(TrafficLightColor.RED);
    });

    it('should set the traffic light color', function () {
        let trafficLight = new TrafficLight();
        expect(trafficLight.color).toEqual(TrafficLightColor.RED);

        trafficLight.color = TrafficLightColor.GREEN;
        expect(trafficLight.color).toEqual(TrafficLightColor.GREEN);
    });
});
