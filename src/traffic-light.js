'use strict';

import {TrafficLightColor} from './traffic-light-color';

export class TrafficLight {
    constructor() {
        this._color = TrafficLightColor.RED;
    }

    set color(color) {
        this._color = color;
    }

    get color() {
        return this._color;
    }
}