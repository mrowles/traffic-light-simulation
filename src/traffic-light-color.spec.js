'use strict';

import {TrafficLightColor} from './traffic-light-color.js';

describe('TrafficLightColor', function () {
    it('should set the traffic light colours', function () {
        let greenTrafficLight = new TrafficLightColor('GREEN');
        let orangeTrafficLight = new TrafficLightColor('ORANGE');
        let redTrafficLight = new TrafficLightColor('RED');

        expect(greenTrafficLight).toEqual(TrafficLightColor.GREEN);
        expect(greenTrafficLight.toString()).toEqual('GREEN');

        expect(orangeTrafficLight).toEqual(TrafficLightColor.ORANGE);
        expect(orangeTrafficLight.toString()).toEqual('ORANGE');

        expect(redTrafficLight).toEqual(TrafficLightColor.RED);
        expect(redTrafficLight.toString()).toEqual('RED');
    });
});
