'use strict';

import {TrafficLight} from './traffic-light';
import {TrafficLightSimulator} from './traffic-light-simulator';

(() => {
    const delay = 1000 * 60 * 5;
    const buffer = 1000 * 30;

    let trafficLightEW = new TrafficLight();
    let trafficLightNS = new TrafficLight();

    let simulator = new TrafficLightSimulator(
        [trafficLightEW, trafficLightNS],
        delay,
        buffer
    );
    simulator.init();
})();