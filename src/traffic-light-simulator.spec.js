'use strict';

import {TrafficLight} from './traffic-light';
import {TrafficLightColor} from './traffic-light-color';
import {TrafficLightSimulator} from './traffic-light-simulator';

describe('TrafficLightSimulator', () => {
    describe('#init()', () => {
        const delay = 300000;
        const buffer = 30000;

        beforeEach(() => {
            jasmine.clock().install();
        });

        afterEach(() => {
            jasmine.clock().uninstall();
        });

        it('should throw an error when not enough traffic lights are set', () => {
            let trafficLightSimulator = new TrafficLightSimulator(
                [],
                delay,
                buffer
            );

            expect(() => {
                trafficLightSimulator.init()
            }).toThrow(new Error('Traffic light simulation requires more than 1 set of lights'));
        });

        it('should begin a simulation for a set of traffic lights', () => {
            let TrafficLightSetNS = new TrafficLight();
            let TrafficLightSetEW = new TrafficLight();

            let trafficLightSimulator = new TrafficLightSimulator(
                [TrafficLightSetNS, TrafficLightSetEW],
                delay,
                buffer
            );

            trafficLightSimulator.init();

            expect(trafficLightSimulator.trafficLights[0].color).toEqual(TrafficLightColor.GREEN);
            expect(trafficLightSimulator.trafficLights[1].color).toEqual(TrafficLightColor.RED);

            jasmine.clock().tick(delay);
            expect(trafficLightSimulator.trafficLights[0].color).toEqual(TrafficLightColor.ORANGE);
            expect(trafficLightSimulator.trafficLights[1].color).toEqual(TrafficLightColor.RED);

            jasmine.clock().tick(buffer);
            expect(trafficLightSimulator.trafficLights[0].color).toEqual(TrafficLightColor.RED);
            expect(trafficLightSimulator.trafficLights[1].color).toEqual(TrafficLightColor.GREEN);

            jasmine.clock().tick(delay);
            expect(trafficLightSimulator.trafficLights[0].color).toEqual(TrafficLightColor.RED);
            expect(trafficLightSimulator.trafficLights[1].color).toEqual(TrafficLightColor.ORANGE);

            jasmine.clock().tick(buffer);
            expect(trafficLightSimulator.trafficLights[0].color).toEqual(TrafficLightColor.GREEN);
            expect(trafficLightSimulator.trafficLights[1].color).toEqual(TrafficLightColor.RED);

            jasmine.clock().tick(delay);
            expect(trafficLightSimulator.trafficLights[0].color).toEqual(TrafficLightColor.ORANGE);
            expect(trafficLightSimulator.trafficLights[1].color).toEqual(TrafficLightColor.RED);

            jasmine.clock().tick(buffer);
            expect(trafficLightSimulator.trafficLights[0].color).toEqual(TrafficLightColor.RED);
            expect(trafficLightSimulator.trafficLights[1].color).toEqual(TrafficLightColor.GREEN);
        });
    });
});